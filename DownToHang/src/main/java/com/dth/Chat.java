package com.dth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by k7lim on 12/1/13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Chat {
    private String message;
    private String author;
    private long timestamp;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Chat() { }

    Chat(String message, String author) {
        this.message = message;
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public String getAuthor() {
        return author;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
