package com.dth;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by k7lim on 12/1/13.
 */
public class EditPlanDialog extends DialogFragment implements TextView.OnEditorActionListener {

    private static final String KEY_PLAN = "EditPlanDialog.plan";
    private static final String KEY_USERNAME = "EditPlanDialog.username";
    private EditText mEditText;
    private Button mSubmitBtn;
    private String mUsername;

    public EditPlanDialog() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_plan, container);
        mEditText = (EditText) view.findViewById(R.id.txt_the_plan);
        getDialog().setTitle("Refine the plan");

        mSubmitBtn = (Button) view.findViewById(R.id.submit_btn);
        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        // Show soft keyboard automatically
        mEditText.requestFocus();
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSubmitBtn.setEnabled(!TextUtils.isEmpty(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        mEditText.setOnEditorActionListener(this);
        mEditText.append(getArguments().getCharSequence(KEY_PLAN));

        mUsername = getArguments().getString(KEY_USERNAME);

        return view;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            submit();
            return true;
        }
        return false;
    }

    private void submit() {
        Firebase plansRef = new Firebase(BuildConfig.FIREBASE_URL).child("planv2").push();
        plansRef.setValue(new Plan(mEditText.getText().toString(), mUsername), new Firebase.CompletionListener() {

            @Override
            public void onComplete(FirebaseError error, Firebase firebase) {
                if (error != null)
                    Log.i("TAGG", String.format("error: %s, code:%d", error.getMessage(), error.getCode()));
                dismiss();
            }
        });
        plansRef.child("timestamp").setValue(ServerValue.TIMESTAMP);
    }

    public static EditPlanDialog newInstance(CharSequence incumbentPlan, String myUsername) {
        EditPlanDialog d = new EditPlanDialog();

        Bundle args = new Bundle(1);
        args.putCharSequence(KEY_PLAN, incumbentPlan);
        args.putString(KEY_USERNAME, myUsername);
        d.setArguments(args);
        return d;
    }
}
