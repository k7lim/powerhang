package com.dth;
import android.app.Activity;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.TextView;
import com.firebase.client.Query;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by k7lim on 12/1/13.
 */
public class ChatListAdapter extends FirebaseListAdapter<Chat> {

    private boolean isTimestampVisible;

    // The username for this client. We use this to indicate which messages originated from this user
    private String username;

    public ChatListAdapter(Query ref, Activity activity, String username) {
        super(ref, Chat.class, R.layout.chat_message, activity);
        this.username = username;
        this.isTimestampVisible = PrefUtils.isTimestampOn();
    }

    /**
     * Bind an instance of the <code>Chat</code> class to our view. This method is called by <code>FirebaseListAdapter</code>
     * when there is a data change, and we are given an instance of a View that corresponds to the layout that we passed
     * to the constructor, as well as a single <code>Chat</code> instance that represents the current data to bind.
     * @param view A view instance corresponding to the layout we passed to the constructor.
     * @param chat An instance representing the current state of a chat message
     */
    @Override
    protected void populateView(View view, Chat chat) {
        // Map a Chat object to an entry in our listview
        String author = chat.getAuthor();
        TextView authorText = (TextView)view.findViewById(R.id.author);
        authorText.setText(author + ": ");
        // If the message was sent by this user, color it differently
        if (author.equals(username)) {
            authorText.setTextColor(Color.RED);
        } else {
            authorText.setTextColor(Color.BLUE);
        }
        ((TextView) view.findViewById(R.id.message)).setText(getMessageText(chat));

    }

    private CharSequence getMessageText(Chat chat) {
        SpannableStringBuilder builder = new SpannableStringBuilder(chat.getMessage());
        if (chat.getTimestamp() > 0 && isTimestampVisible) {
            builder.append(" ");
            int start = builder.length();
            builder.append(getTimestampString(chat));
            int end = builder.length();
            builder.setSpan(new RelativeSizeSpan(.65f), start, end, SpannableStringBuilder.SPAN_INCLUSIVE_INCLUSIVE);
            builder.setSpan(new ForegroundColorSpan(Color.argb(128, 25, 25, 25)), start, end, SpannableStringBuilder.SPAN_INCLUSIVE_INCLUSIVE);
        }

        return builder;
    }

    private CharSequence getTimestampString(Chat chat) {
        return SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT).format(new Date(chat.getTimestamp()));
    }
}