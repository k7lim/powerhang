package com.dth;

import android.text.Editable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by k7lim on 12/26/13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan {
    private String plan;
    private String author;
    private long timestamp;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Plan() { }

    Plan(String plan, String author) {
        this.plan = plan;
        this.author = author;
    }

    public String getPlan() {
        return plan;
    }

    public String getAuthor() {
        return author;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
