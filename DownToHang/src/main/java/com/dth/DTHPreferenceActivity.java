package com.dth;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

/**
 * Created by k7lim on 12/7/13.
 */
public class DTHPreferenceActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        Preference versionPref = findPreference("appVersion");
        versionPref.setSummary(DTHApplication.getVersionString());
    }
}
