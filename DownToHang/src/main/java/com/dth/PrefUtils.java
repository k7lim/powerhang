package com.dth;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by k7lim on 12/1/13.
 */
public class PrefUtils {

    /// CHAT IDENTITY STORAGE/PREFERENCES
    private static SharedPreferences getChatPrefs() {
        return DTHApplication.get().getSharedPreferences("ChatPrefs", Context.MODE_PRIVATE);
    }

    public static void setUsername(String newname) {
        getChatPrefs().edit().putString("username", newname).commit();
    }

    public static String getUsername() {
        return getChatPrefs().getString("username", null);
    }


    // SETTINGS

    private static SharedPreferences getSettingsPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(DTHApplication.get());
    }

    public static boolean isNotificationSoundOn() {
        return getSettingsPrefs().getBoolean("notificationSoundOn", true);
    }

    public static String getNotificationSoundKey() {
        return getSettingsPrefs().getString("notificationSoundKey", "notif1");
    }

    public static boolean isTimestampOn() {
        return getSettingsPrefs().getBoolean("timestampsOn", true);
    }

}
