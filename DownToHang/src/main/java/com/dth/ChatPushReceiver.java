package com.dth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by k7lim on 12/1/13.
 */
public class ChatPushReceiver extends BroadcastReceiver {
    private static final String TAG = "ChatReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getAction();
            String channel = intent.getExtras().getString("com.parse.Channel");
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            Log.d(TAG, "got action " + action + " on channel " + channel + " with:");
            Iterator itr = json.keys();
            while (itr.hasNext()) {
                String key = (String) itr.next();
                Log.d(TAG, "..." + key + " => " + json.getString(key));
            }

            String senderName = json.getString("username");
            if (!TextUtils.equals(senderName, PrefUtils.getUsername()) && !MainActivity.isInForeground()) {
                String chat = json.getString("chat");
                NotificationService.notify(context, senderName, chat);
            }



        } catch (JSONException e) {
            Log.d(TAG, "JSONException: " + e.getMessage());
        }
    }
}
