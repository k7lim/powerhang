package com.dth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by k7lim on 12/1/13.
 */
public class UpgradeNoticeReceiver extends BroadcastReceiver {
    private static final String TAG = "UpgradeNoticeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {

            int newVersion;
            String newVersionString;
            String changeLog;

            Bundle extras = intent.getExtras();

            JSONObject json;
            if (extras.containsKey("com.parse.Data")) {
                json = new JSONObject(extras.getString("com.parse.Data"));
                newVersion = json.optInt("newVersionCode");
                newVersionString = json.optString("newVersionString");
                changeLog = json.optString("changeLog");

                Log.i(TAG, String.format("newVersion:%d, name:%s, change:%s", newVersion, newVersionString, changeLog));
                if (newVersion > DTHApplication.getVersionCode()) {
                    NotificationService.notifyUpgrade(context, newVersionString, changeLog);
                }
            }

        } catch (JSONException e) {
            Log.w(TAG, "JSONException: " + e.getMessage());
        }
    }
}
