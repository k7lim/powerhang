package com.dth;

/**
 * Created by newuser on 12/7/13.
 */
public class ReceivedSharedTextEvent {
    final String text;

    public ReceivedSharedTextEvent(String text) {
        this.text = text;
    }
}
