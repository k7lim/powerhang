package com.dth;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.firebase.client.*;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SendCallback;
import com.squareup.otto.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

/**
 * Created by k7lim on 12/1/13.
 */
public class ChatFragment extends Fragment {

    private String username;
    private Firebase ref;
    private ValueEventListener connectedListener;
    private ChatListAdapter chatListAdapter;

    @InjectView(R.id.planPane)
    TextView planWiki;

    @InjectView(R.id.chatList)
    ListView listView;

    @InjectView(R.id.messageInput)
    EditText inputText;

    @InjectView(R.id.sendButton)
    ImageButton sendButton;


    public static ChatFragment newInstance() {
        ChatFragment f =  new ChatFragment();

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, result);

        setupUsername();

        planWiki.setMovementMethod(new ScrollingMovementMethod());
        Firebase plansRef = new Firebase(BuildConfig.FIREBASE_URL).child("planv2");
        Query latestPlanQuery = plansRef.limit(1);
        latestPlanQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snapshot, String s) {
                Object value = snapshot.getValue(Plan.class);
                planWiki.setText(((Plan) value).getPlan());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError error) {
            }
        });

        ref = new Firebase(BuildConfig.FIREBASE_URL).child("chatv3");
        inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEND ||
                        actionId == EditorInfo.IME_ACTION_DONE ||
                        keyEvent.getAction() == KeyEvent.ACTION_DOWN &&
                                keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    sendMessage();
                }
                return true;
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        return result;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        // Tell our list adapter that we only want 50 messages at a time
        chatListAdapter = new ChatListAdapter(ref.limit(50), getActivity(), username);
        listView.setAdapter(chatListAdapter);
        chatListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatListAdapter.getCount() - 1);
            }
        });

        // Finally, a little indication of connection status
        connectedListener = ref.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected && isAdded()) {
                    sendButton.setEnabled(true);
                    getActivity().setTitle(getString(R.string.app_name));
                } else {
                    sendButton.setEnabled(false);
                    getActivity().setTitle(getString(R.string.offline));
                }
            }

            @Override
            public void onCancelled(FirebaseError error) {
                // No-op
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        DTHApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        DTHApplication.getBus().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        ref.getRoot().child(".info/connected").removeEventListener(connectedListener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.chat_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            showEditForm();
            return true;
        } else if (item.getItemId() == R.id.action_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, planWiki.getText().toString());
            startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.action_share)));
            return true;
        } else if (item.getItemId() == R.id.action_settings && isAdded()) {
            Intent prefsIntent = new Intent(getActivity(), DTHPreferenceActivity.class);
            startActivity(prefsIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void showEditForm() {
        FragmentManager fm = getFragmentManager();
        EditPlanDialog editPlanDialog = EditPlanDialog.newInstance(planWiki.getText(), username);
        editPlanDialog.show(fm, "fragment_edit_plan");
    }

    @Subscribe
    public void onUsernameChange(UsernameChangedEvent e) {
        setupUsername();
    }

    private void setupUsername() {
        username = PrefUtils.getUsername();
        if (username == null) {
            Random r = new Random();
            // Assign a random user name if we don't have one saved.
            username = "Randar" + r.nextInt(100000);
            showUsernameDialog();
        }
    }

    private void showUsernameDialog() {
        FragmentManager fm = getFragmentManager();
        EditNameDialog editNameDialog = EditNameDialog.newInstance();
        editNameDialog.show(fm, "fragment_edit_name");
    }


    private void sendMessage() {
        final String input = inputText.getText().toString();
        if (!input.equals("")) {
            // Create our 'model', a Chat object
            Chat chat = new Chat(input, username);
            // Create a new, auto-generated child of that chat location, and save our chat data there
            Firebase newChatRef = ref.push();
            newChatRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    sendPush(input);
                    inputText.setText("");
                }

                @Override
                public void onCancelled(FirebaseError error) {

                }
            });

            newChatRef.setValue(chat);
            newChatRef.child("timestamp").setValue(ServerValue.TIMESTAMP);
        }
    }

    private void sendPush(String input) {
        try {
            JSONObject data = new JSONObject();
            data.put("action", getString(R.string.chat_push_action));
            data.put("username", username);
            data.put("chat", input);

            ParsePush push = new ParsePush();
            push.setData(data);
            push.setExpirationTimeInterval(60 * 60 * 3);
            push.setChannel("Chats");
            push.sendInBackground(new SendCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        Log.i("TAGG", e.getMessage() + "code:" + e.getCode());
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Subscribe
    public void onTextShared(ReceivedSharedTextEvent e) {
        Log.i("ChatFragment", "received text:" + e.text);
        inputText.append(e.text);
        DTHApplication.getBus().post(new ConsumedSharedTextEvent());
    }

}
