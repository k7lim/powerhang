package com.dth;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by k7lim on 12/1/13.
 */
public class NotificationService {
    private final static int NOTIFY_ID = 0x77; // static number so all replace themselves
    private final static int UPGRADE_ID = 0x74; // static number so all replace themselves

    public static void notify(Context context, String senderName, String chat) {
        NotificationManager mgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// Sets an ID for the notification, so it can be updated
        long[] pattern = {500, 500, 500};


        Intent notificationIntent = new Intent(context, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Log.i("CHAT", "chat:" + chat);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(senderName)
                .setContentText(chat)
                .setAutoCancel(true)
                .setLights(context.getResources().getColor(R.color.led), 500, 500)
                .setVibrate(pattern)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_launcher);

        if (PrefUtils.isNotificationSoundOn()) {
            builder.setSound(getSelectedNotificationSound(), AudioManager.STREAM_NOTIFICATION);
        }

        mgr.notify(NOTIFY_ID, builder.build());
    }


    private static final String ASSET_URI_PREFIX = "android.resource://com.dth/raw/";
    private final static Map<String, Uri> SOUND_URIS;

    static {
        SOUND_URIS = new HashMap<String, Uri>();
        SOUND_URIS.put("system", RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        SOUND_URIS.put("notif1", Uri.parse(ASSET_URI_PREFIX + "notif1"));
        SOUND_URIS.put("notif2", Uri.parse(ASSET_URI_PREFIX + "notif2"));
        SOUND_URIS.put("notif3", Uri.parse(ASSET_URI_PREFIX + "notif3"));
        SOUND_URIS.put("notif4", Uri.parse(ASSET_URI_PREFIX + "notif4"));
    }



    public static Uri getSelectedNotificationSound() {
        return getNotificationSound(PrefUtils.getNotificationSoundKey());
    }

    public static Uri getNotificationSound(String key) {
        return SOUND_URIS.get(key);
    }


    public static void clear(Context context) {
        NotificationManager mgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.cancel(NOTIFY_ID);
    }

    public static void notifyUpgrade(Context context, String newVersionString, String description) {
        NotificationManager mgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// Sets an ID for the notification, so it can be updated
        long[] pattern = {300, 300, 300};

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bit.ly/powerhang"));

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, browserIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.update_available, newVersionString))
                .setContentText(description)
                .setAutoCancel(true)
                .setLights(context.getResources().getColor(R.color.led), 500, 500)
                .setVibrate(pattern)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_launcher);

        if (PrefUtils.isNotificationSoundOn()) {
            builder.setSound(getSelectedNotificationSound(), AudioManager.STREAM_NOTIFICATION);
        }

        mgr.notify(UPGRADE_ID, builder.build());
    }
}
