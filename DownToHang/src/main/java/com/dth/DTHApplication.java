package com.dth;

import android.app.Application;
import android.content.pm.PackageManager;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.squareup.otto.Bus;

import java.util.Map;

/**
 * Created by k7lim on 12/1/13.
 */
public class DTHApplication extends Application {

    private static DTHApplication app;
    private static Bus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        Parse.initialize(this, BuildConfig.PARSE_APPID, BuildConfig.PARSE_CLIENTKEY);
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        PushService.setDefaultPushCallback(this, MainActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();
        PushService.subscribe(this, "Chats", MainActivity.class);
        bus = new Bus();

        new Firebase(BuildConfig.FIREBASE_URL).child("latestVersion").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot == null)
                    return;

                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value == null)
                    return;

                Long code = (Long) value.get("code");
                if (code != null && code > DTHApplication.getVersionCode()) {
                    NotificationService.notifyUpgrade(DTHApplication.this, (String) value.get("string"), (String) value.get("changeLog"));
                }
            }

            @Override
            public void onCancelled(FirebaseError error) {
                //nothin
            }
        });
    }

    public static DTHApplication get() {
        return app;
    }

    public static Bus getBus() {
        return bus;
    }

    public static int getVersionCode() {
        try {
            return app.getPackageManager().getPackageInfo(app.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return Integer.MAX_VALUE;
        }
    }

    public static String getVersionString() {
        try {
            String versionName = app.getPackageManager().getPackageInfo(app.getPackageName(), 0).versionName;

            return versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
    