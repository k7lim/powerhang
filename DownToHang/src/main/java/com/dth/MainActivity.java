package com.dth;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

public class MainActivity extends ActionBarActivity {

    private static boolean inForeground = false;
    private String mSharedText;

    public static boolean isInForeground() {
        return inForeground;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, ChatFragment.newInstance())
                    .commit();

            Intent intent = getIntent();
            handleIntent(intent);
        }


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        String type = intent.getType();
        Log.i("MainActivity", "action:" + action);
        Log.i("MainActivity", "type:" + type);

        if (Intent.ACTION_SEND.equals(action) && TextUtils.equals("text/plain", type)) {
            mSharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
            Log.i("MainActivity", "mSharedText:" + mSharedText);
            if (!TextUtils.isEmpty(mSharedText))
                DTHApplication.getBus().post(new ReceivedSharedTextEvent(mSharedText));
        } else {
            mSharedText = null;
        }
    }


    @Produce
    public ReceivedSharedTextEvent produceLastSharedText() {
        if (TextUtils.isEmpty(mSharedText))
            return null;

        return new ReceivedSharedTextEvent(mSharedText);
    }

    @Subscribe
    public void onSharedTextConsumed(ConsumedSharedTextEvent e) {
        mSharedText = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        NotificationService.clear(this);
        inForeground = true;
    }

    @Override
    protected void onStop() {
        inForeground = false;
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        DTHApplication.getBus().register(this);
    }

    @Override
    protected void onPause() {
        DTHApplication.getBus().unregister(this);
        super.onPause();
    }
}
